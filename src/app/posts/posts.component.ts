import { Component, OnInit } from '@angular/core';
import { PostsService } from './posts.Service';

@Component({
  selector: 'jce-posts',
  templateUrl: './posts.component.html',
    styles: [`
    .users li { cursor: default; }
    .users li:hover { background: #ecf0f1; } 
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }
  `]
})
export class PostsComponent implements OnInit {

  isLoading:Boolean = true;
  posts;
  currentPost;
  select(post){
    this.currentPost = post;
  }
  constructor(private _postsService:PostsService) { }

  deletePost(post){
    //this.posts.splice(
      //this.posts.indexOf(post),1
   // )
   this._postsService.deletePost(post);
  }



  addPost(post){
    //this.posts.push(post)
     this._postsService.addPost(post);
  }

  updatePost(post){
       this._postsService.updatePost(post);
   }

  ngOnInit() {
    //this._postsService.getPosts().subscribe(postsData=> this.posts=postsData);
    this._postsService.getPosts().subscribe(usersData => 
    {this.posts = usersData;
    this.isLoading = false});
  }

}
