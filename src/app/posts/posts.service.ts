import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import { AngularFire } from 'angularfire2';

@Injectable()
export class PostsService {

  //getPosts() {
  //let posts = [
  //  {title: 'A', content:'AAA', author:'Avi'},
  // {title: 'B', content:'BBB', author:'Beni'},
  //  {title: 'C', content:'CCC', author:'Celi'},
  //  {title: 'D', content:'DDD', author:'Dani'},
  // ]
  //  return posts;
  // }

  postsObservable;

  //private _url = 'https://jsonplaceholder.typicode.com/posts';
  getPosts() {
    //return this._http.get(this._url).map(res =>res.json()).delay(2000);
    //this.postsObservable = this.af.database.list('/posts');
    //return this.postsObservable;
      this.postsObservable = this.af.database.list('/posts').map(
    posts =>{
      posts.map(
        post => {
          post.userName = [];
          for(var p in post.users){
            post.userName.push(
            this.af.database.object('/users/' + p)
            )
          }
        }
      );
      return posts;
   }
  )
  return this.postsObservable;
  }

  addPost(post) {
    this.postsObservable.push(post);
  }

  updatePost(post) {
    let postKey = post.$key;
    let postData = { body: post.body, title: post.title };
    this.af.database.object('/posts/' + postKey).update(postData);
  }

  deletePost(post) {
    let postKey = post.$key;
    this.af.database.object('/posts/' + postKey).remove();
  }

  constructor(private af: AngularFire) { }
}
