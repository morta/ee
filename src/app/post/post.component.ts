import { Component, OnInit, EventEmitter, Output  } from '@angular/core';
import {Post} from './post';


@Component({
  selector: 'jce-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  inputs:['post']
})
export class PostComponent implements OnInit {

  @Output() deleteEvent = new EventEmitter<Post>();
  @Output() editEvent = new EventEmitter<Post>();
  sendDelete(){
    this.deleteEvent.emit(this.post);
  }
  post:Post;
  isEdit : boolean = false;
  editButtonText = 'Edit';
  constructor() { }

  tempPost:Post = {id:null,title:null,body:null};

  cancelEdit(){
    this.isEdit = false;
    this.post.title = this.tempPost.title;
    this.post.body = this.tempPost.body;
    this.editButtonText = 'Edit'; 
  }

  toggleEdit(){
     this.isEdit = !this.isEdit;
     this.isEdit ?  this.editButtonText = 'Save' : this.editButtonText = 'Edit';
      if(!this.isEdit){
        this.editEvent.emit(this.post);
      }
    // if(this.isEdit){
    //   this.tempPost.id = this.post.id;
     //  this.tempPost.title = this.post.title;
    //   this.tempPost.body = this.post.body;
   //  } else {
    //   let originalAndNew = [];
    //   originalAndNew.push(this.tempPost,this.post);
    //   this.editEvent.emit(originalAndNew);
  //   }
      }

  ngOnInit() {
  }

}
