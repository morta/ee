import { Component, OnInit } from '@angular/core';
import 'rxjs/add/operator/delay';
import { UsersService } from './users.service';

@Component({
  selector: 'jce-users',
  templateUrl: './users.component.html',
  //styleUrls: ['./users.component.css']
  styles: [`
    .users li { cursor: default; }
    .users li:hover { background: #ecf0f1; } 
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }
  `]
})
export class UsersComponent implements OnInit {

  //users = [
   // {name:'Jhon', email:'jhon@gmail.com'},
    //{name:'Jack', email:'jack@gmail.com'},
  //  {name:'Alice', email:'alice@gmail.com'}
  //]
  
  currentUser;
  users;
  

  select(user){
    this.currentUser = user;
    console.log(this.currentUser);
  }

  addUser(user){
     this.users.push(user)
  }
  constructor(private _usersService:UsersService) { }

  ngOnInit() {
    this._usersService.getUsers().subscribe(usersData => this.users = usersData);
  }

}
