import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {AngularFire} from 'angularfire2';

@Injectable()
export class UsersService {
//private _url = 'https://jsonplaceholder.typicode.com/users';

usersObservable;
getUsers() {
  //return this._http.get(this._url).map(res =>res.json())
  this.usersObservable = this.af.database.list('/users');
  return this.usersObservable;
}

  constructor(private af:AngularFire) { }

}
