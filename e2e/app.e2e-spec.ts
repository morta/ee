import { AngStudyPage } from './app.po';

describe('ang-study App', function() {
  let page: AngStudyPage;

  beforeEach(() => {
    page = new AngStudyPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
